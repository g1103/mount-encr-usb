#!/bin/bash

sudo cryptsetup luksOpen $(blkid| grep crypt | awk -F ":" '{print $1}') encr_usb
sudo mkdir -p /usb && sudo mount /dev/mapper/encr_usb /usb
cd /usb
ls -lh

